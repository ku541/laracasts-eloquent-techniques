<?php

use Faker\Generator as Faker;

$factory->define(App\Lesson::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence,
        'body' => $faker->paragraph,
        'views' => $faker->randomNumber,
        'length' => $faker->randomNumber,
        'difficulty' => $faker->randomElement([
            'beginner',
            'intermediate',
            'advanced'
        ])
    ];
});
