<?php

namespace App\Http\Controllers;

use App\Document;

class DocumentController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  \App\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function show(Document $document)
    {
        return view('documents.show')->withDocument($document);
    }
}
