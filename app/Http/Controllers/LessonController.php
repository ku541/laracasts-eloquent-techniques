<?php

namespace App\Http\Controllers;

use App\Lesson;
use App\LessonFilter;

class LessonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(LessonFilter $filter)
    {
        return Lesson::filter($filter)->get();
    }
}
