<?php

namespace App;

class LessonFilter extends QueryFilter
{
    protected $builder;

    public function popular($order = 'desc')
    {
        return $this->builder->orderBy('views', $order);
    }

    public function difficulty($level)
    {
        return $this->builder->where('difficulty', $level);
    }

    public function length($order = 'asc')
    {
        return $this->builder->orderBy('length', $order);
    }

    public function take($count)
    {
        return $this->builder->take($count);
    }
}
