<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\QueryFilter;

class Lesson extends Model
{
    public function scopeFilter($query, QueryFilter $filter)
    {
        return $filter->apply($query);
    }
}
