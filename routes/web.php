<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

auth()->loginUsingId(1);

Route::get('/', function () {
    return view('welcome');
});

Route::resource('documents', 'DocumentController')->only('show');
Route::resource('lessons', 'LessonController')->only('index');
